﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medicines.Models;

namespace Medicines.Controllers
{
    public class HomeController : Controller
    {
        MedicineContext db = new MedicineContext();

        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult AddAgent()
        {
            return View();
        }


        [HttpPost]
        public ActionResult AddAgent(Medicine m)
        {
            db.Medicines.Add(m);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult EditAgent(int id)
        {
            var agent = db.Medicines.Find(id);
            return View(agent);
        }


        [HttpPost]
        public ActionResult EditAgent(Medicine agent)
        {
            var temp_agent = db.Medicines.Find(agent.Id);
            temp_agent.MedType = agent.MedType;
            temp_agent.Name = agent.Name;
            temp_agent.Price = agent.Price;
            temp_agent.Description = agent.Description;
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        //?
        [HttpGet]
        [ActionName("DeleteAgent")]  
        public ActionResult ConfirmDeleteAgent(int id)
        {
            var agent = db.Medicines.Find(id);
            return View(agent);
        }


        [HttpPost]
        public ActionResult DeleteAgent(int Id)
        {
            var temp_agent = db.Medicines.Find(Id);
            db.Medicines.Remove(temp_agent);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AgentsTable()
        {
            List<Medicine> AgentsList = new List<Medicine>();

            foreach (Medicine m in db.Medicines)
            {
                Medicine Temp = new Medicine
                {
                    Id = m.Id,
                    MedType = m.MedType,
                    Name = m.Name,
                    Price = m.Price,
                    Description = m.Description                   
                };
                AgentsList.Add(Temp);
            }

            return View(AgentsList);
        }


        [HttpPost]
        public ActionResult SearchAgents(string s)
        {
            //if (s == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            List<Medicine> AgentsList = new List<Medicine>();

            s.Trim();
            s.ToLower();

            if (s == "")
            {
                foreach (Medicine m in db.Medicines)
                {
                    Medicine Temp = new Medicine
                    {
                        Id = m.Id,
                        MedType = m.MedType,
                        Name = m.Name,
                        Price = m.Price,
                        Description = m.Description
                    };
                    AgentsList.Add(Temp);
                }
            }

            string TempStr;
            foreach (var m in db.Medicines)
            {
                TempStr = string.Copy(m.Name);
                TempStr.ToLower();
                if (TempStr.StartsWith(s))
                {
                    Medicine Temp = new Medicine
                    {
                        Id = m.Id,
                        MedType = m.MedType,
                        Name = m.Name,
                        Price = m.Price,
                        Description = m.Description
                    };
                    AgentsList.Add(Temp);
                }
            }

            return View(AgentsList);
        }


        public ActionResult Recipe()
        {
            foreach (var MaD in db.MedicineAndDosages)
                db.MedicineAndDosages.Remove(MaD);
            db.SaveChanges();
            return View();
        }


        [HttpGet]
        public ActionResult AddRecipeAgent()
        {
            var allAgents = db.Medicines.ToList();
            return View(allAgents);
        }


        [HttpPost]
        public ActionResult AddRecipeAgent(MedicineAndDosage agent)
        {
            db.MedicineAndDosages.Add(agent);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);           
        }


        [HttpGet]
        [ActionName("DeleteRecipeAgent")]
        public ActionResult ConfirmDeleteRecipeAgent(int id)
        {
            var agentRecipe = db.MedicineAndDosages.Find(id);
            return View(agentRecipe);
        }


        [HttpPost]
        public ActionResult DeleteRecipeAgent(int Id)
        {
            var agentRecipe = db.MedicineAndDosages.Find(Id);
            db.MedicineAndDosages.Remove(agentRecipe);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AgentsRecipeTable()
        {
            List<MedicineAndDosage> AgentsRecipeList = new List<MedicineAndDosage>();

            foreach (var m in db.MedicineAndDosages)
            {
                MedicineAndDosage Temp = new MedicineAndDosage
                {
                    Id = m.Id,
                    MedType = m.MedType,
                    Name = m.Name,
                    Price = m.Price,
                    Description = m.Description,
                    Number = m.Number,
                    Gauge = m.Gauge
                };
                AgentsRecipeList.Add(Temp);
            }

            return View(AgentsRecipeList);
        }


        public ActionResult Recipes()
        {
            return View();
        }


        [HttpPost]
        public ActionResult RecipesTable()
        {
            List<Recipe> RecipesList = new List<Recipe>();

            foreach (var m in db.Recipes)
            {
                Recipe Temp = new Recipe
                {
                    Id = m.Id,
                    RecipeName = m.RecipeName,
                    MedicineAndDosages = m.MedicineAndDosages
                };
                RecipesList.Add(Temp);
            }

            return View(RecipesList);
        }


        [HttpGet]
        public ActionResult CreateRecipe()
        {
            var allMaDs = db.MedicineAndDosages.ToList();
            return View(allMaDs);

        }


        [HttpPost]
        public ActionResult CreateRecipe(Recipe recipe)
        {
            db.Recipes.Add(recipe);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Notes()
        {
            return View();
        }
    }

}