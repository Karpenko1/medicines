﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Medicines.Models
{
    public class MedicineAndDosage
    {
        [Key]
        public int Id { get; set; }
        public int MedType { get; set; }
        [Required]
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        //Number of g/mg
        public int Number { get; set; }
        public Gage Gauge { get; set; }

        public virtual ICollection<Recipe> Recipes { get; set; }
    }

    public enum Gage : byte
    {
        [Display(Description="gramm")]
        g,
        [Display(Description = "milligramm")]
        mg,
        [Display(Description = "microgramm")]
        mkg
    }
}