﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Medicines.Models
{
    public class MedicinesDbInitializer : DropCreateDatabaseAlways<MedicineContext>
    {
        protected override void Seed(MedicineContext db)
        {
            db.Medicines.Add(new Medicine { MedType = 1, Name = "First agent", Price = 33, Description = "aaaa" });
            db.Medicines.Add(new Medicine { MedType = 2, Name = "Second agent", Price = 44, Description = "bbbbb" });
            db.Medicines.Add(new Medicine { MedType = 3, Name = "Third agent", Price = 55, Description = "cccccc" });

            MedicineAndDosage MAD1 = new MedicineAndDosage { Id = 1, Name = "Aspirin", Number = 11, Gauge = Gage.g };
            MedicineAndDosage MAD2 = new MedicineAndDosage { Id = 2, Name = "Paracetamol", Number = 22, Gauge = Gage.g };
            MedicineAndDosage MAD3 = new MedicineAndDosage { Id = 3, Name = "Loratadine", Number = 33, Gauge = Gage.g };

            db.MedicineAndDosages.Add(MAD1);
            db.MedicineAndDosages.Add(MAD2);
            db.MedicineAndDosages.Add(MAD3);
    
            Recipe R1 = new Recipe
            {
                Id = 1,
                RecipeName = "Recipe A",
                MedicineAndDosages = new List<MedicineAndDosage>() { MAD1, MAD2 }
            };
            Recipe R2 = new Recipe
            {
                Id = 2,
                RecipeName = "Recipe B",
                MedicineAndDosages = new List<MedicineAndDosage>() { MAD1, MAD3 }
            };
            Recipe R3 = new Recipe
            {
                Id = 3,
                RecipeName = "Recipe C",
                MedicineAndDosages = new List<MedicineAndDosage>() { MAD2, MAD3 }
            };

            db.Recipes.Add(R1);
            db.Recipes.Add(R2);
            db.Recipes.Add(R3);

            base.Seed(db);
        }
    }
}