﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Medicines.Models
{
    public class Recipe
    {
        [Key]
        public int Id { get; set; }
        public string RecipeName { get; set; } 

        public virtual ICollection<MedicineAndDosage> MedicineAndDosages { get; set; }
    }
}