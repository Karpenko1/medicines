﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Medicines.Models
{
    public class MedicineContext : DbContext
    {
        public DbSet<Medicine> Medicines { get; set; }
        public DbSet<MedicineAndDosage> MedicineAndDosages { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Recipe>().HasMany(recipe => recipe.MedicineAndDosages)
                .WithMany(mad => mad.Recipes)
                .Map(t => t.MapLeftKey("RecipeId")
                .MapRightKey("MedicineAndDosageId")
                .ToTable("RecipeMAD"));
        }
    }
}